# wak-htmlprag.mk - build dorodango packages from this distribution
# Copyright 2011 David Banks <amoebae@gmail.com>; license LGPL 3 or later.

# Basic smoke test for create-bundle
test:
	@tmp_d=$$(mktemp -dt wak-htmlprag-XXXXXXXX); \
	tmp_f=$$(mktemp -t wak-htmlprag-XXXXXXXX.zip); \
	rsync -rt --exclude-from exclude.rsf ./ $$tmp_d; \
	doro create-bundle -o $$tmp_f $$tmp_d; \
	unzip -l $$tmp_f; \
	rm -rf $$tmp_d $$tmp_f

build:
	@tmp_d=$$(mktemp -dt wak-htmlprag-XXXXXXXX); \
	tmp_f=$$(mktemp -t wak-htmlprag-XXXXXXXX.zip); \
	rsync -rt --exclude-from exclude.rsf ./ $$tmp_d; \
	doro create-bundle -o $$tmp_f $$tmp_d
